-- ProbablyEngine Rotation Packager
-- Custom Furry Warrior Rotation MACKS + Winner Winnington + Credits to AKE & SimFury & Matt & Everyone
-- Created on Nov 3rd 2013 2:00 pm
ProbablyEngine.rotation.register_custom(72, "Probably_WinnerWinnington", {
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------UTILITIES---------------------------
----------------------------------------------------------------
----------------------------------------------------------------

  -- Stance Dance
  { "71", { "player.health <= 10", "player.seal != 2" }},
  { "2457", {	"player.health >= 26", "player.seal != 1" }},
    
  --Trinket Procs
  { "#trinket1" },
  { "#trinket2" },
  
  --Healthstone 5512
  { "#5512", "player.health < 45"},
  
  -- Forsaken/Escape Artist (Get Outta That Shit)
  { "20589", "player.state.root" },
  { "20589", "player.state.snare" },
  
  --Agro
  { "Intimidating Shout", "target.threat >= 80" },
	
  -- Hotkeys (Use these if you like them I dont)
  -- { "Heroic Leap", "modifier.alt", "ground" },
  { "pause", "modifier.lalt" },
  -- { "Shattering Throw", "modifier.ralt" },
  -- { "Ravager", "modifier.lalt", "ground" },
  -- { "Intimidating Shout", "modifier.control" },

  -- Buffs
  { "Battle Shout", "!player.buffs.attackpower" },
  {"Berserker Rage", {"!player.buff(Enrage)", "player.lastcast(Bloodthirst)", "player.buff(Raging Blow).count < 2" } },

  
  -- Survival
  { "Die by the Sword", "player.health < 60" },
  { "Rallying Cry", "player.health < 25" },
  { "Victory Rush", "player.buff(Victorious)" },
  { "Impending Victory", "player.health <= 80" },
  
----------------------------------------------------------------
----------------------------------------------------------------
--------------------------COOLDOWNS-----------------------------
----------------------------------------------------------------
----------------------------------------------------------------
{{

-- berserker_rage,if=buff.enrage.down|(prev_gcd.bloodthirst&buff.raging_blow.stack<2)
{"Berserker Rage", {"!player.buff(Enrage)", "player.lastcast(Bloodthirst)","player.buff(Raging Blow).count < 2" } },
-- use_item,name=vial_of_convulsive_shadows,if=(active_enemies>1|!raid_event.adds.exists)&((talent.bladestorm.enabled&cooldown.bladestorm.remains=0)|buff.recklessness.up|target.time_to_die<25|!talent.anger_management.enabled)  Tricky Trinket Usage, mabe in the future

-- potion,name=draenic_strength,if=(target.health.pct<20&buff.recklessness.up)|target.time_to_die<=25
{ "#109219", { "player.buff(Recklessness)", "target.ttd <= 25" } },

-- recklessness,if=(((target.time_to_die>190|target.health.pct<20)&(buff.bloodbath.up|!talent.bloodbath.enabled))|target.time_to_die<=12|talent.anger_management.enabled)&((talent.bladestorm.enabled&(!raid_event.adds.exists|enemies=1))|!talent.bladestorm.enabled)
{"Recklessness", { "target.ttd > 190", "player.buff(Bloodbath)" } },
{"Recklessness", { "target.ttd > 190", "!talent(6,2)" } },
{"Recklessness", { "target.health < 20", "player.buff(Bloodbath)" } },
{"Recklessness", { "target.health < 20", "!talent(6,2)" } },
{"Recklessness", { "target.ttd <=12", "talent(7,1)", "talent(6,3)", "player.spell(Bladestorm).cooldown < 0" } },
{"Recklessness", { "target.ttd <=12", "talent(7,1)", "!talent(6,3)" } },
--avatar,if=buff.recklessness.up|cooldown.recklessness.remains>60|target.time_to_die<30
{"Avatar", {"player.buff(Recklessness)", "target.ttd < 30" } },

--blood_fury,if=buff.bloodbath.up|!talent.bloodbath.enabled|buff.recklessness.up
{"Blood Fury", {"player.buff(Bloodbath)", "player.buff(Recklessness)" } },

--berserking,if=buff.bloodbath.up|!talent.bloodbath.enabled|buff.recklessness.up
{"Berserking", {"player.buff(Bloodbath)", "player.buff(Recklessness)" } },

--arcane_torrent,if=rage<rage.max-40
{"Arcane Torrent", {"player.rage > 60" } },

}, "modifier.cooldowns" },

----------------------------------------------------------------
----------------------------------------------------------------
-------------------------------AOE------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
{{-- Four Or More Targets 

	--?actions.aoe=bloodbath
	{ "Bloodbath" },                                                                          

	--?actions.aoe+=/bloodthirst,if=buff.enrage.down|rage<50|buff.raging_blow.down
	{ "Bloodthirst", { "!player.buff(Enrage)" } },                                            
	{ "Bloodthirst", { "!player.buff(Raging Blow)" } },
	
	--?actions.aoe+=/raging_blow,if=buff.meat_cleaver.stack>=3&buff.enrage.up
	{ "Raging Blow", { "player.buff(Meat Cleaver).count >= 3 ", "player.buff(Enrage)" } },
	{ "Raging Blow", { "!player.buff(Raging Wind)", "player.buff(Enrage)" } },
--	{ "Raging Blow", { "player.buff(Raging Blow).count = 2", "!player.buff(Raging Wind)", "player.buff(Enrage)" } },
	
	--?actions.aoe+=/whirlwind
	{ "Whirlwind", {"player.buff(Raging Wind)", "player.buff(Enrage)"} },                                                                          	
	{ "Whirlwind", {"player.buff(Meat Cleaver).count < 3", "player.buff(Enrage)"} },

	--?actions.aoe+=/ravager,if=buff.bloodbath.up|!talent.bloodbath.enabled
	{ "Ravager", nil, "target.ground", { "player.buff(Bloodbath)" } },                                              
	{ "Ravager", nil, "target.ground", { "!talent(6, 2)" } },

	--?actions.aoe+=/raging_blow,if=buff.meat_cleaver.stack>=3
--	{ "Raging Blow", { "player.buff(Meat Cleaver).count >= 3 " } },                           

	--?actions.aoe+=/recklessness,sync=bladestorm
--	{ "Recklessness", { "player.buff(Bladestorm)" } },                                        

	--?actions.aoe+=/bladestorm,if=buff.enrage.remains>6
--	{ "Bladestorm", { "player.buff(Enrage).duration > 6", "player.buff(Enrage)" } },          
	{ "Bladestorm", "player.buff(Enrage).duration > 6" },          

	--?actions.aoe+=/execute,if=buff.sudden_death.react
	{ "Execute", "player.buff(Sudden Death)" },                                           

	--?actions.aoe+=/dragon_roar,if=buff.bloodbath.up|!talent.bloodbath.enabled
	{ "Dragon Roar", { "player.buff(Bloodbath)" } },                                          
--	{ "Dragon Roar", { "!talent(6, 2) " } },

	--?actions.aoe+=/bloodthirst
	{ "Bloodthirst" },                                                                        

	--?actions.aoe+=/wild_strike,if=buff.bloodsurge.up
--	{ "Wild Strike", { "player.buff(Bloodsurge)" } },                                         

}, "modifier.multitarget",},	

----------------------------------------------------------------
----------------------------------------------------------------
--------------------------SINGLE TARGET-------------------------
----------------------------------------------------------------
----------------------------------------------------------------
--bloodbath
{"Bloodbath"},

--recklessness,if=target.health.pct<20&raid_event.adds.exists
--wild_strike,if=(rage>rage.max-20)&target.health.pct>20
{"Wild Strike", {"player.rage > 90", "target.health > 20" } },

--bloodthirst,if=(!talent.unquenchable_thirst.enabled&(rage<rage.max-40))|buff.enrage.down|buff.raging_blow.stack<2
{"Bloodthirst", {"!talent(3,3)", "player.rage > 60", "!player.buff(Enrage)", "player.buff(Raging Blow).count > 2" } },
 
--ravager,if=buff.bloodbath.up|(!talent.bloodbath.enabled&(!raid_event.adds.exists|raid_event.adds.in>60|target.time_to_die<40))
{"Ravager", {"player.buff(Bloodbath)", "target.ttd > 40" } },

--siegebreaker
{"Siegebreaker"},

--execute,if=buff.sudden_death.react
{"Execute", {"player.buff(Sudden Death)"} },

--storm_bolt
{"Storm Bolt"},

--wild_strike,if=buff.bloodsurge.up
{"Wild Strike", "player.buff(Bloodsurge)" },

--execute,if=buff.enrage.up|target.time_to_die<12
{"Execute", {"player.buff(Enrage)", "target.ttd < 12" } },

--dragon_roar,if=buff.bloodbath.up|!talent.bloodbath.enabled
{"Dragon Roar", "player.buff(Bloodbath)" },

--raging_blow
{"Raging Blow"},

--wait,sec=cooldown.bloodthirst.remains,if=cooldown.bloodthirst.remains<0.5&rage<50
--wild_strike,if=buff.enrage.up&target.health.pct>20
{"Wild Strike", { "player.buff(Enrage)", "target.health > 20" } },

--bladestorm,if=!raid_event.adds.exists
--shockwave,if=!talent.unquenchable_thirst.enabled
--impending_victory,if=!talent.unquenchable_thirst.enabled&target.health.pct>20
--bloodthirst
{"Bloodthirst"}

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------ End DPS ------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

}, {

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--------------------------------------------------------------------------------------------- Out Of Combat ---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

{ "Battle Shout", "!player.buffs.attackpower" },

},

   function()

end)