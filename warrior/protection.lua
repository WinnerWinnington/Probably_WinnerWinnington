-- SPEC ID 73
ProbablyEngine.rotation.register_custom(73, "Probably_WinnerWinnington", {

--Utilities

-- Pause Rotation
	{ "pause", "modifier.lshift" },

-- leap
	--{ "Heroic Leap", "modifier.control", "mouseover.ground" },
	
-- banner
	--{ "Mocking Banner", "modifier.lalt", "mouseover.ground" },

-- Buffs
	{ "6673", "!player.buff(6673)" }, -- Battle Shout
	
-- auto target stuff
	{ "/targetenemy [noexists]", { "toggle.autotarget", "!target.exists" } },
	{ "/targetenemy [dead]", { "toggle.autotarget", "target.exists", "target.dead" } },

-- Survival
	-- self heals
	{ "Victory Rush", "player.health < 85" },
	{ "Impending Victory", "player.health < 70" },
	{ "#5512", "player.health < 40" }, --Healthstone
	{ "Enraged Regeneration", "player.health < 70" },
	{ "shield wall", "player.health < 50" },
	{ "last stand", "player.health < 20" },
	{ "114192", "player.health < 20" },
	
-- Kicks
	{ "Pummel", "target.interruptAt(30)" },


-- Cooldowns
--{{
--	{ "Bloodbath" },
--	{ "Avatar" },
--	{ "Recklessness", "target.boss" },
--},a{ "modifier.cooldowns", "target.range <= 8" } },
-- Procs

	{ "Heroic Strike", "player.buff(Unyielding Strikes).count = 6" },
	{ "Heroic Strike", "player.buff(Ultimatum)", "target"},
  	{ "Shield Slam", "player.buff(Sword and Board)", "target"},
	
	-- AoE
{{
	{ "Shockwave" },
	{ "Thunder Clap", "!target.debuff(Deep Wounds)" },
	{ "Bladestorm" },
}, { "modifier.multitarget", "target.range <= 5" } }, 

	-- single target
{{
	{ "Shield Charge", "!player.buff(Shield Charge)" },
	{ "18499", {"!player.buff(12880)", "player.buff(169667)"} }, --Berserker Rage
	{ "Shield Slam" },
	{ "Heroic Strike", "player.buff(Shield Charge)" },
	{ "Heroic Strike", "player.rage > 94" },
	{ "Revenge" },
	{ "Heroic Strike", "player.buff(Unyielding Strikes).count > 3" },
	{ "Bloodbath" },
	{ "Dragon Roar" },
	{ "Devastate" },
	--{ "Execute", "player.buff(Sudden Death)" },
	{ "Execute", {"player.rage > 80", "target.health <=20", "!player.buff(Shield Charge)"}  },	
}, "target.range <= 8" },
	
}, {

-- leap
	--{ "Heroic Leap", "modifier.control", "mouseover.ground"},

},
 function ()
ProbablyEngine.toggle.create('autotarget', 'Interface\\Icons\\ability_hunter_snipershot', 'Auto Target', 'Automaticaly target the nearest enemy when target dies or does not exist.')
ProbablyEngine.toggle.create('tc', 'Interface\\Icons\\ability_deathwing_bloodcorruption_death', 'Threat Control', '')
  end)